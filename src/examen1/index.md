---
layout: layouts/page.njk
title: Évaluation Technologie Web pour la mobilité 2022
date: Last Modified
---

> Un de vos clients veut développer une application mobile de copilotage. Des pilotes d'avions privés s'enregistrent sur un site et proposent des trajets entre différentes villes.

On vous demande de produire la maquette d'une **application web mobile** qui servira de prototype à montrer au client.
- Les services web de tests sont déjà opérationnels.
- Vous utiliserez [OnsenUI](https://onsen.io/) pour développer cette maquette.

Vous trouverez les icones de l'application dans le fichier <a href="icon.zip" download>icon.zip</a>.

La couleur dominante de l'application est : `#e5604d`.

Vous vous efforcerez de produire une **progressive web app** qui peut s'afficher sur les téléphones de dernière génération. Cependant au stade la maquette il n'est pas demandé de réaliser le _service worker_.

## 1. Écran d'accueil
Cette écran est constitué de la liste des pilotes enregistrés sur la plateforme.

Titre de l'écran : Pilotes

:warning: La capture n'est pas totalement conforme au cahier des charges concernant le titre

![](captures/page1.png)

## 2. Écran de détail

Titre de l'écran : le nom du pilote

:warning: La capture n'est pas totalement conforme au cahier des charges concernant le titre

L'écran de détail est composé de 3 onglets (tabbar)
- Destinations : icone flight-takeoff, et un badge contenant le nombre de destinations
- Billet : icone confirmation-number
- Places : icone airline-seat-recline-extra

## 2.1 Onglet Destinations

Une carte centrée sur la ville du pilote, un marqueur indique le nom du pilote et des lignes les différentes destinations.

![](captures/page2.png)

## 2.2 Onglet Billet

Les informations relatives au billet sont affichées sous forme d'une grille css de 5 lignes et de 3 colonnes

La première ligne le code de l'aéroport de départ, une icone d'avion (fichier <a href="logo_plane.svg" download>logo_plane</a>) et le code d'aéroport d'arrivée. Taille de la police 26px

La deuxième ligne un qrcode centré contenant le texte : `FRA 448161651 ORY`

Les 2, 3 et 4e lignes : sur la première colonne le titre de l'information, sur les colonnes 2 et 3 les informations du billet.

![](captures/page3.png)

## 2.3 Onglet Places

Une carte des places disponibles dans l'avions avec différentes couleurs pour représenter les disponibilités.

![](captures/page4.png)

---

# Travail à faire

1. Réaliser une maquette globale de l'application. Commencer par intégrer de manière statique (sans javascript) les différentes pages et onglets.

2. Ajouter les élements des différentes pages, toujours de manière statique :
- liste des pilotes
- tableau du billet
- plan de l'avion
- qrcode
- carte

3. Déclarer l'application comme une **progressive web app**.

4. Rendre dynamique les différents élements avec **javascript**.

## 1. La liste des pilotes

Données depuis le webservice [https://workshop.neotechweb.net/ws/flighter/pilotes.php](https://workshop.neotechweb.net/ws/flighter/pilotes.php)

1.a Il faut **trier** les pilotes par pays et **filtrer** ceux qui ont un nombre de destinations supérieur à 0.

1.b Afficher une rupture avec le pays. De même que le drapeau du pays [https://workshop.neotechweb.net/ws/flags/64/fr.png](https://workshop.neotechweb.net/ws/flags/64/fr.png) en utilisant le code du pays.

1.c Les photos des pilotes sont situées à l'url [https://workshop.neotechweb.net/ws/flighter/users/1.png](https://workshop.neotechweb.net/ws/flighter/users/1.png) en utilisant l'id du pilote.


## 2. Carte des destinations

Données depuis le webservice [https://workshop.neotechweb.net/ws/flighter/trajets.php](https://workshop.neotechweb.net/ws/flighter/trajets.php)

2.a Une carte **centrée** sur la ville du pilote (premières coordonnées latitude/longitude du fichier) représenté par un **marqueur**, vous indiquez le nom du pilote dans un **popup** lorsque l'on clique sur le marqueur.

2.b Les différentes destinations sont représentées en partant de la ville du pilote et par les différentes coordonnées du tableau `destinations`. Une ligne par groupe de longitude/latitude du tableau.


## 3. Onglet Billet

La page est statique, il n'y a rien de particulier à faire si ce n'est la mise en page.

Le texte du QRCode est `FRA 448161651 ORY`.

Vous utiliserez la librairie <a href="qrcode.min.js" download>qrcode.min.js</a>

La documentation est disponible à l'adresse suivante : https://davidshimjs.github.io/qrcodejs/

## 4. Onglet Places

Cet écran affiche le plan de l'avion (fichier <a href="plane.svg" download>plane.svg</a>), l'utilisateur est invité à cliquer sur le plan pour choisir sa place.

Données depuis le webservice [https://workshop.neotechweb.net/ws/flighter/places.php](https://workshop.neotechweb.net/ws/flighter/places.php)

La disposition des siège est dynamique et diffère d'un appareil à l'autre.

- Créer un symbole représentant un siège.
- Placez les sièges dans le groupe `seats`, suivant une grille espacée de 60px pour chaque rangée (`row`) et de 110px pour chaque côté `(L:0px, R:110px)`.

```svg
<path
  d="m 44.748544,9.0737864 c -0.1,-0.1 -0.5,-3 -3.5,-3.3 -0.6,-3.8 -5,-4.6 -5,
  -4.6 0,0 -17.8,0 -24.6,0 -5.1,0 -6.3,3.1 -6.5,4.6 -3.1,0.4 -3.6,3.3 -3.6,
  3.3 0,0 0,27.6 0,35.5 0,7.9 6.8,8 6.8,8 0,0 23.5,0 30.3,0 6.8,0 6.2,-7.2 6.2,
  -7.2 0,0 0,-36.1 -0.1,-36.3 z" />
<path fill="currentColor"
  d="M 38.568738,53.378835 H 8.3687379 c -0.1,0 -7.6,-0.1 -7.6,-8.7 V 9.1788349
  c 0.2,-1.4 1.5,-4.1 5.1,-4.1 3.2,0 4.8,1.4 4.8,4.2 V 38.678835 c 0.3,0.7 2.6,
  5 12.2,5 10.5,0 13.2,-4.7 13.4,-5.1 V 9.2788349 c 0.1,-1.5 1.1,-4.2 4.4,-4.2 2.4,
  0 3.9,1.2 4.6,3.7 0,0.1 0,0.1 0,0.1 v 0 c 0.2,0.9 0.2,5.5 0.1,36.5 0,0.1 0.3,
  3.6 -1.9,6 -1,1.3 -2.7,2 -4.9,2 z M 2.3687379,9.2788349 V 44.578835 c 0,7 5.8,
  7.1 6,7.1 H 38.568738 c 1.7,0 3,-0.5 3.9,-1.5 1.7,-1.9 1.5,-4.8 1.5,-4.9 0.1,
  -13.7 0.1,-34.2 0,-36 0,-0.1 0,-0.1 -0.1,-0.2 -0.4,-1.2 -1,-2.6 -3.1,-2.6 -2.7,
  0 -2.8,2.5 -2.8,2.6 V 38.378835 c 0,1.3 -3.9,6.6 -15,6.6 -11.6,0 -13.6999998,
  -5.9 -13.7999998,-6.2 v -0.1 -29.6 c 0,-0.9 0,-2.6 -3.2,-2.6 -2.9,0.2 -3.5,2.4 -3.6,2.8 z" />
<path fill="currentColor"
  d="m 4.3687379,6.7788349 c 0,-0.1 -0.1,-2.4 1.6,-4.2 1.3,-1.4 3.2,-2 5.7,
  -2 h 24.7 c 2,0.3 5.8,2 5.8,6.2 l -1.6,-0.2 c -0.1,-3.2 -3.8,-4.3 -4.4,-4.4 h -24.5
  c -2,0 -3.6,0.5 -4.6,1.5 -1,1.1 -1.2,2.6 -1.2,3 z" />
```

Colorier les siège suivant le statut (`state`)
|state|remplissage|couleur|
|----:|-----------|-------|
|    0| #fff      |#bdbdbd|
|    1| #ffcc00   |#e2a62a|
|    2| #E26154   |#c14437|

Lorsque l'utilisateur clique sur un siège avec l'état 0 celui-ci passe à l'état 1. Pour les autres états il ne se passe rien.
