---
title: Exercice Flutter
date: Last Modified
---


Concevoir une application Flutter 
Un menu latéral permettant 
- d'accéder à sa wantslist
- un lecteur de Codebarre pour accéder à un disque en particulier

La barre de menu contient de l'écran d'accueil contient une zone de recherche pour chercher un artiste

```http
GET https://api.discogs.com/database/search?q=Ottawan&type=artist
Authorization: Discogs token=HQbDeyffXGfkKLBCViolfEjWwfdsDrRahgAhFArh
```

Lorsque l'on clique sur l'artiste on affiche les oeuvres

```http
GET https://api.discogs.com/artists/82844/releases?sort=year
Authorization: Discogs token=HQbDeyffXGfkKLBCViolfEjWwfdsDrRahgAhFArh
```

Puis les versions de cette oeuvre

```http
GET https://api.discogs.com/masters/26474/versions
Authorization: Discogs token=HQbDeyffXGfkKLBCViolfEjWwfdsDrRahgAhFArh
```

Et enfin on peut ajouter à la wantslist une oeuvre

```http
PUT https://api.discogs.com/users/i2m/wants/1755740
Authorization: Discogs token=HQbDeyffXGfkKLBCViolfEjWwfdsDrRahgAhFArh
```

GET https://api.discogs.com/database/search?q=Nirvana&country=france&title=nevermind
Authorization: Discogs token=HQbDeyffXGfkKLBCViolfEjWwfdsDrRahgAhFArh


###
GET https://api.discogs.com/artists/82844
Authorization: Discogs token=HQbDeyffXGfkKLBCViolfEjWwfdsDrRahgAhFArh


###


###
GET https://api.discogs.com/masters/26474
Authorization: Discogs token=HQbDeyffXGfkKLBCViolfEjWwfdsDrRahgAhFArh

###


###


###
GET https://api.discogs.com/database/search?q=Nirvana&country=france&title=nevermind
Authorization: Discogs token=HQbDeyffXGfkKLBCViolfEjWwfdsDrRahgAhFArh



###
