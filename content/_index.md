---
title: ""
date: 2023-01-18T21:41:59+01:00
draft: false
---

## Prérequis

+ Savoir installer un environnement pour [données embarquées](https://programmation-embarquee.netlify.app/)
+ Connaître Les langages du [web](https://integration-documents-web.netlify.app/) : HTML, CSS, Javascript et SVG


## Applications Web

[Progressive Web App](pwa)

### Héberger une site web

- Installer le serveur web [Nginx](http/nginx)

- Protéger la communication avec le protocole HTTPS et un [certificat TLS](security/certificat).

- Fonctionner en mode [hors connexion](pwa/webworker)

## Développement hybride avec Flutter

[Introduction](flutter/)

Le langage [Dart](flutter/dart)

[Flutter](flutter)

[UI](flutter/ui)

Consommation d'un [Web API](flutter/future)

[Lecteur de code barre](flutter/barcode)

[Carte](flutter/carte)
